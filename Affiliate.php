<?php

class Affiliate
{
    public $name;
    public $balance = [];
    public $upperAffiliate = null;
    public $subAffiliates = [];
    public $customers = [];

    public $totalMoney;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setBalance($balance): void
    {
        array_push($this->balance, $balance);

    }

    public function getBalance()
    {
        if ($this->name = 'John') {
            $this->totalMoney = (array_sum($this->balance) * 5) / 100;
            return 'The money ' . $this->name . ' can get: ' . $this->totalMoney . '<br>';
        }
        $this->totalMoney = (array_sum($this->balance) * 10) / 100;
        return 'The money ' . $this->name . ' can get: ' . $this->totalMoney . '<br>';
    }

    public function setUpperAffiliate($newAffiliate)
    {
        $this->upperAffiliate = $newAffiliate;
    }

    public function getUpperAffiliate()
    {
        return $this->upperAffiliate;
    }

    public function subAffiliate($subAffiliate)
    {
        return array_push($this->subAffiliates, $subAffiliate);
    }

    public function customer($customer)
    {
        return array_push($this->customers, $customer);
    }

    public function refer($customer)
    {
        if ($this->upperAffiliate == null) {
            array_push($this->customers, $customer);
        } else {
            array_push($this->subAffiliates, $customer);
        }
    }

    public function withDraw($affiliate, $numberMoneyGet)
    {
        if ($affiliate < 100) {
            return $this->name . ' money smaller than 100 so you can get money :( ' . '<br>';
        } elseif ($affiliate - $numberMoneyGet < 0) {
            return $this->name . ' account not enough for  ' . $numberMoneyGet . '. Your account left is ' . $this->totalMoney . '<br>';
        } elseif ($affiliate > 100) {
            $moneyLeft = $affiliate - $numberMoneyGet;
            return 'Success! ' . $this->name . ' can get your money' . ' .Money left on your account is: ' . $moneyLeft . '<br>';
        }

    }


    public function printSubAff()
    {
        for ($i = 0; $i < count($this->subAffiliates); $i++) {
            echo ' The affiliate of ' . $this->name . ' introduct is ' . $this->subAffiliates[$i] . '<br>';

        }
    }

    public function printCustomers()
    {
        for ($i = 0; $i < count($this->customers); $i++) {
            echo $this->name . ' customer is ' . $this->customers[$i] . '<br><br>';
        }
    }
}
