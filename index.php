<?php
include 'Order.php';
include 'Affiliate.php';
include 'StoreOwner.php';
include 'Customer.php';

$storeMoyes = new StoreOwner();
$storeMoyes->setName('Store Moyes');
echo $storeMoyes->getName() . '<br>';

$affiliateJohn = new Affiliate('John');
echo $affiliateJohn->getName() . ' the first Affiliate <br><br>';
$affiliateJohn->refer('Sahar');
$affiliateJohn->subAffiliate('Sahar');
$affiliateJohn->refer('Eva');
$affiliateJohn->subAffiliate('Eva');
$affiliateJohn->refer('Jimmy');
$affiliateJohn->subAffiliate('Jimmy');
$affiliateJohn->refer('Henry');
$affiliateJohn->subAffiliate('Henry');
echo $affiliateJohn->printCustomers();

$affiliateSahar = new Affiliate('Sahar');
$affiliateSahar->refer('customer 1');
echo $affiliateSahar->printCustomers();

$affiliateEva = new Affiliate('Eva');
$affiliateEva->refer('customer 2');
echo $affiliateEva->printCustomers();

$affiliateJimmy = new Affiliate('Jimmy');
$affiliateJimmy->refer('customer 3');
echo $affiliateJimmy->printCustomers();

$affiliateHenry = new Affiliate('Henry');
$affiliateHenry->refer('customer 4');
echo $affiliateHenry->printCustomers();

$customer1 = new Customer('Sahar');
$customer1->setName('customer 1');

$customer2 = new Customer('Eva');
$customer2->setName('customer 2');

$customer3 = new Customer('Jimmy');
$customer3->setName('customer 3');


$customer4 = new Customer('Henry');
$customer4->setName('customer 4');

$billOfCustomer1 = new Order();
$billOfCustomer1->setTotal(800, $customer1->getName());
echo $billOfCustomer1->getTotal();
echo $customer1->placeOrder($billOfCustomer1->total);


$billOfCustomer2 = new Order();
$billOfCustomer2->setTotal(800, $customer2->getName());
echo $billOfCustomer2->getTotal();
echo $customer2->placeOrder($billOfCustomer2->total);

$billOfCustomer3 = new Order();
$billOfCustomer3->setTotal(800, $customer3->getName());
echo $billOfCustomer3->getTotal();
echo $customer3->placeOrder($billOfCustomer3->total);

$billOfCustomer4 = new Order();
$billOfCustomer4->setTotal(800, $customer4->getName());
echo $billOfCustomer4->getTotal();
echo $customer4->placeOrder($billOfCustomer4->total);

echo $affiliateJohn->printSubAff();

$storeMoyes->setBalance($billOfCustomer1->total);
$storeMoyes->setBalance($billOfCustomer2->total);
$storeMoyes->setBalance($billOfCustomer3->total);
$storeMoyes->setBalance($billOfCustomer4->total);
echo $storeMoyes->getBalance() . '<br>';

$affiliateJohn->setBalance($billOfCustomer1->total);
echo $affiliateJohn->getBalance();
$affiliateJohn->setBalance($billOfCustomer2->total);
echo $affiliateJohn->getBalance();
$affiliateJohn->setBalance($billOfCustomer3->total);
echo $affiliateJohn->getBalance();
$affiliateJohn->setBalance($billOfCustomer4->total);
echo $affiliateJohn->getBalance();

echo $affiliateJohn->withDraw($affiliateJohn->totalMoney, 300);
echo $affiliateJohn->withDraw($affiliateJohn->totalMoney, 150);
echo $affiliateSahar->withDraw($affiliateSahar->totalMoney, 50);

$totalAff = $customer1->getYourMoney() + $customer2->getYourMoney() + $customer3->getYourMoney() + $customer4->getYourMoney();

$storeMoyes->printBalance($affiliateJohn->totalMoney, $totalAff);
