<?php


class Order
{
    public $total;
    public $customer;

    public function setTotal(int $totalBill, $customer)
    {
        $this->total = $totalBill;
        $this->customer = $customer;
    }

    public function getTotal()
    {
        return 'The bill of ' . $this->customer . ' is ' . $this->total . '<br>';
    }
}
