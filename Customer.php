<?php

class Customer
{
    private $name;
    private $address;
    private $affiliate = null;

    private $yourMoney;

    public function __construct($affiliate)
    {
        $this->affiliate = $affiliate;
    }

    public function setName($nameCustomer)
    {
        $this->name = $nameCustomer;
    }

    public function getName()
    {
        return $this->name;
    }

    public function placeOrder($totalMoney)
    {
        $this->yourMoney = ($totalMoney * 10) / 100;
        return 'The money of ' . $this->affiliate . ' can get: ' . $this->yourMoney . '<br><br>';
    }

    public function getYourMoney()
    {
        return $this->yourMoney;
    }

}
